#ifndef DATA_H
#define DATA_H

enum Symbol {
    S1, S2, S3, S4, S5, S6, S7, S8, SYMBOLS_NUMBER, NO_SYMBOL
};

inline double justVariable(double variable) {return variable;}
inline double variableSquare(double variable) {return variable * variable;}
inline double indexVariable(double variable) {return variable > 0;}



#endif /* DATA_H */

