#include <iostream>

#include "Configuration.h"

// <editor-fold defaultstate="collapsed" desc="CONSTRUCTIONS & SETTERS">

Configuration::Configuration(ScoresTable &scoresTable, LineSet &lineSet)
: scoresTable_(scoresTable), lineSet_(lineSet), cylindersNumber_(scoresTable.getCylindersNumber()) {
    //TODO check weather ScoreTable and LineSet number of cylinders are equal
}

Configuration::Configuration(ScoresTable &scoresTable, LineSet &lineSet, double bet, double paybackPercExp, double paybackPercStandDev, int gamesNumber, double hitExp)
: Configuration(scoresTable, lineSet) {
    bet_ = bet;
    paybackPercentExpectation_ = paybackPercExp;
    paybackPercentStandDeviation_ = paybackPercStandDev;
    hitExpectation_ = hitExp;
}

Configuration::~Configuration() {
}
//////////////

void Configuration::setTargetBet(double bet) {
    bet_ = bet;
}

void Configuration::setTargetPaybackPercentExpectation(double expectation) {
    paybackPercentExpectation_ = expectation;
}

void Configuration::setTargetPaybackPersentStandDev(double standDev, int gamesNumber) {
    paybackPercentStandDeviation_ = standDev;
    paybackPercentGamesNumber_ = gamesNumber;
}

void Configuration::setTargetHitExp(double expectation) {
    hitExpectation_ = expectation;
}

void Configuration::setTargetVariablesValues() {
    targetPaybackExpect_ = paybackPercentExpectation_ * bet_ * correction;
    targetPaybackDisp_ = pow(bet_ * paybackPercentStandDeviation_, 2) * paybackPercentGamesNumber_ * correction;
    targetHitExp_ = hitExpectation_ * correction;
    printConfiguration();
}
// </editor-fold>

SlotMachine Configuration::generateSlotMashine(int minSymbolsPerCylinder, double precision, int tries) {
    int (*distr)[SYMBOLS_NUMBER] = generateSymbolsDistributions(24, 0.00011, 1000);

    std::cout << "Cylinders generation finished." << "\n";

    std::vector<Cylinder> cylinders;
    for (int i = 0; i < cylindersNumber_; i++) {
        cylinders.push_back(Cylinder(*(distr + i)));
        std::cout << cylinders.back();
    }

    printCalculatedParameters(distr);

    delete[] distr;

    SlotMachine machine(scoresTable_, lineSet_, cylinders);
    return machine;
}

// <editor-fold defaultstate="collapsed" desc="GENERATORS">
int (*Configuration::generateSymbolsDistributions(int minSymbolsPerCylinder, double precision, int tries))[SYMBOLS_NUMBER] {
    setTargetVariablesValues();
    int triesCounter = 0;

    printMachineParameters(minSymbolsPerCylinder, precision, tries);
    std::cout << "Cylinders generation started." << "\n";
    
    int (*distr)[SYMBOLS_NUMBER] = generateDistributionsApproximation(minSymbolsPerCylinder);
    int (*optDistr)[SYMBOLS_NUMBER] = generateDistributionsApproximation(minSymbolsPerCylinder);
    double gainExp, gainDisp, hitExp;
    calculateTargetVariables(distr, gainExp, gainDisp, hitExp);
    double approach = estimateApproachToTargetValues(gainExp, gainDisp, hitExp);

    do {
        bool improved = findOptimisatedDistribution(distr, optDistr, approach);
        if (improved) {
            std::copy(optDistr[0], optDistr[cylindersNumber_], distr[0]);
        } else {
            int * end = distr[0] + (cylindersNumber_ * SYMBOLS_NUMBER);
            for (int * p = distr[0]; p < end; ++p) {
                ++(*p);
            }
            calculateTargetVariables(distr, gainExp, gainDisp, hitExp);
            approach = estimateApproachToTargetValues(gainExp, gainDisp, hitExp);
        }

    } while (tries - ++triesCounter > 0 && approach > precision);

    delete[] optDistr;

    return distr;
}

bool Configuration::findOptimisatedDistribution(int (*distr)[SYMBOLS_NUMBER], int (*optDistr)[SYMBOLS_NUMBER], double &minAapproach) {
    double gainExp, gainDisp, hitExp;
    calculateTargetVariables(distr, gainExp, gainDisp, hitExp);
    bool improved = false;

    for (int c = 0; c < cylindersNumber_; c++) {
        for (int s = 0; s < SYMBOLS_NUMBER; s++) {
            for (int d = -2; d < 3; d += 1) {
                distr[c][s] += d;
                if (lineSet_.isAppropriateSymbolDistribution(distr)) {
                    calculateTargetVariables(distr, gainExp, gainDisp, hitExp);
                    double approach = estimateApproachToTargetValues(gainExp, gainDisp, hitExp);
                    if (approach < minAapproach) {
                        minAapproach = approach;
                        std::copy(distr[0], distr[cylindersNumber_], optDistr[0]);
                        improved = true;
                    }
                }
                distr[c][s] -= d;
            }
        }
    }

    return improved;
}

int (*Configuration::generateDistributionsApproximation(int symbolsPerCylinder))[SYMBOLS_NUMBER] {
    int (*distr)[SYMBOLS_NUMBER] = new int[cylindersNumber_][SYMBOLS_NUMBER];
    memset(distr, 0, cylindersNumber_ * SYMBOLS_NUMBER * sizeof (int));
    for (int c = 0; c < cylindersNumber_; c++) {
        int counter = 0;
        while (counter < symbolsPerCylinder) {
            ++distr[c][counter++ % SYMBOLS_NUMBER];
        }
    }
    return distr;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="TARGET VALUES CALCULATIONS">

void Configuration::calculateTargetVariables(int (*distr)[SYMBOLS_NUMBER], double &gainExp, double &gainDisp, double &hitExp) {
    double (*prob)[SYMBOLS_NUMBER] = lineSet_.calculateProbabilities(distr);

    calculateExpectations(prob, &justVariable, gainExp);
    calculateExpectations(prob, &variableSquare, gainDisp);
    gainDisp -= gainExp * gainExp;
    calculateExpectations(prob, indexVariable, hitExp);

    delete[] prob;
}

void Configuration::calculateExpectations(double (*probabilities)[SYMBOLS_NUMBER], double(*scoreToRandomVariableFunc)(double), double &expectation) {
    expectation = 0;
    for (int c = 0; c <= cylindersNumber_; c++) {
        for (int s = 0; s < SYMBOLS_NUMBER; s++) {
            double score = (*scoreToRandomVariableFunc)(scoresTable_.getScore(static_cast<Symbol> (s), c));
            expectation += probabilities[c][s] * score;
        }
    }
}

double Configuration::estimateApproachToTargetValues(const double &gainExp, const double &gainDisp, const double &hitExp) {
    return (pow((gainExp - targetPaybackExpect_) / targetPaybackExpect_, 2)
            + pow((gainDisp - targetPaybackDisp_) / targetPaybackDisp_, 2)
            + pow((hitExp - targetHitExp_) / targetHitExp_, 2)) / 3;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="OTHER">

void Configuration::printDistr(int (*distr)[SYMBOLS_NUMBER]) {
    for (int c = 0; c < cylindersNumber_; c++) {
        std::cout << '[';
        for (int s = 0; s < SYMBOLS_NUMBER; s++) {
            std::cout << distr[c][s] << ',';
        }
        std::cout << ']';
    }
    std::cout << '\n';
    //    std::cout << gainExpectationTargetFunc(distr) << "\t" << gainDispersionTargetFunc(distr) << "\t" << hitExpectationTargetFunc(distr) << "\n";
}

void Configuration::printConfiguration() {
    std::cout << "CONFIGURATION: " << "\n";
    std::cout << "Specified: " << "\n";
    std::cout << "\t" << "Target payback percent expectation: " << "\t" << paybackPercentExpectation_ << "\n";
    std::cout << "\t" << "Target payback percent st.deviation: " << "\t" << paybackPercentStandDeviation_ << "\n";
    std::cout << "\t" << "Target hit expectation: " << "\t" << hitExpectation_ << "\n";
    std::cout << "Derivatives: " << "\n";
    std::cout << "\t" << "Single game gain expectation: " << "\t" << targetPaybackExpect_ << "\n";
    std::cout << "\t" << "Single game gain dispersion: " << "\t" << targetPaybackDisp_ << "\n";
    std::cout << "\t" << "Target hit expectation: " << "\t" << targetHitExp_ << "\n";
}

void Configuration::printMachineParameters(int minSymbolsPerCylinder, double precision, int tries) {
    std::cout << "MACHINE PARAMETERS: " << "\n";
    std::cout << "\t" << "Minimal symbols number per cylinder: " << "\t" << minSymbolsPerCylinder << "\n";
    std::cout << "\t" << "Precision: " << "\t" << precision << "\n";
    std::cout << "\t" << "Maximal number of iterations: " << "\t" << tries << "\n";
}

void Configuration::printCalculatedParameters(int (*distr)[SYMBOLS_NUMBER]) {
    double gainExp, gainDisp, hitExp;
    calculateTargetVariables(distr, gainExp, gainDisp, hitExp);
    std::cout << "CALCULATED PARAMETERS: " << "\n";
    std::cout << "\t" << "Single game gain expectation: " << "\t" << gainExp << "\n";
    std::cout << "\t" << "Single game gain dispersion: " << "\t" << gainDisp << "\n";
    std::cout << "\t" << "Target hit expectation: " << "\t" << hitExp << "\n";
}


// </editor-fold>
