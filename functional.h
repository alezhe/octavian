#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include <sstream>
#include <string>

#include "data.h"
#include "Configuration.h"
#include "Mashine.h"

void demoTest();
void runPaybackTest(SlotMachine &machine);
void runPaybackStDevTest(SlotMachine &machine);
void runTest(SlotMachine &machine, int games, double &payBack, int &hits);

void development();


#endif /* FUNCTIONAL_H */

