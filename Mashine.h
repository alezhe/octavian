#ifndef MASHINE_H
#define MASHINE_H

#include <iostream>
#include <stdlib.h>
#include <random>
#include <vector>

#include "data.h"
#include "ScoresTable.h"
#include "Line.h"
#include "Cylinder.h"

class SlotMachine {
protected:
    std::mt19937 generator;
    const ScoresTable &scoresTable_;
    LineSet &lineSet_;
    std::vector<Cylinder> cylinders_;

    const unsigned int cylindersNumber_;
    const unsigned int windowSize_;
public:
    SlotMachine(const ScoresTable &scoresTable, LineSet &lineSet, std::vector<Cylinder> cylinders);
    virtual ~SlotMachine();
public: //GETTERS
    int getCylindersNumber();
public:
    double play();
    void playAndPrint();
private:
    Symbol* generateCombination();
    double defineGain(Symbol* combination);
    int getMatches(Symbol* combination, Symbol s);

    void printCombination(Symbol* combination);
};

#endif /* MASHINE_H */

