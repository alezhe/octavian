#ifndef LINE_H
#define LINE_H

#include <iostream>
#include <bitset>
#include <map>
#include <vector>
#include <numeric>

#include "data.h"

class LineSet {
    // <editor-fold defaultstate="collapsed" desc="Line class">
public:
    class Line {
    protected:
        LineSet * const pLineSet_;
        long long lineCode_;
        int combinationCode_;
        int lineLength_;

    public:
        Line(LineSet * const parent, long long lineCode, int number);
        Line(LineSet * const parent, long long lineCode);
        Line(const Line& orig);
        virtual ~Line();

    private:
        int defineLineLength();
        
    public:
        Line* conjuctWithLine(Line *pLine);
        int getPossibleLength();
        int getCombinationCode();
        int getPositionInWindow(int c);

        friend std::ostream& operator<<(std::ostream& os, const Line& line);
    };
    // </editor-fold>

protected: //MEMBERS
    const unsigned int cylindersNumber_;
    const unsigned int windowSize_;
    int codeCounter = 1;
    std::map<int, std::vector<Line*>> lines_;
public: //CONSTRUCTION
    LineSet(unsigned int cylindersNumber, unsigned int windowSize);
    LineSet(unsigned int cylindersNumber, unsigned int windowSize, unsigned int linesNumber, const int *codedLines);
    virtual ~LineSet();
public:
    void addLine(long long lineCode);
    void printSet();
public: //GETTERS
    int getWindowSize();
public: //SERVICES
    bool isAppropriateSymbolDistribution(int (*distr)[SYMBOLS_NUMBER]);
    double (*calculateProbabilities(int (*distr)[SYMBOLS_NUMBER]))[SYMBOLS_NUMBER];
private: //PROBABILITY CALCULATIONS
    std::vector<Line*> defineLinesCombinations(int matches);
    unsigned long long countPermutationsNumber(int (*distr)[SYMBOLS_NUMBER], int * symbolsNumbers, Symbol symbol, int matchs, std::vector<LineSet::Line*> matchLines);

    int* countSymbolsInCylinders(int (*distr)[SYMBOLS_NUMBER]);
    unsigned long long countTotalPermutationsNumber(int* symbolsNumbers);
    int defineForbidenSymbolsNumber(Line *pLine, int matchs);
public: //GAME
    int getMatches(Symbol* combination, Symbol target);
};

#endif /* LINE_H */

