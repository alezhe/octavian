#include "functional.h"

const int CYLINDERS_NUMBER = 5;

const int WINDOW_SIZE = 3;
const double BET = 5;
const double TARGET_RETURN_PERSCENT = 0.92;
const double TARGET_RETURN_PERSCENT_ST_DEV = 0.007;
const int TARGET_RETURN_PERSCENT_GAMES_NUMBER = 300000;
const double TARGET_HIT = 0.2;


const int MIN_SYMBOLS_PER_CYLINDER = 21;
const int MAX_ITERATIONS_NUMBER = 1000;
const double PRECIOUS_ST_DEV = 0.00011;

double SCORES[CYLINDERS_NUMBER + 1][SYMBOLS_NUMBER] = {
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {3, 5, 7, 9, 12, 15, 20, 30},
    {15, 30, 50, 60, 75, 90, 120, 150},
    {45, 75, 150, 250, 350, 500, 750, 1000}
};

int ENCODED_LINES[]{9362, 18724, 4681, 17492, 5393};

void demoTest() {
    LineSet lineSet(CYLINDERS_NUMBER, WINDOW_SIZE, sizeof (ENCODED_LINES) / sizeof (int), &ENCODED_LINES[0]);
    ScoresTable scoreTable(CYLINDERS_NUMBER, SCORES[0]);
    Configuration configuration(scoreTable, lineSet, BET,
            TARGET_RETURN_PERSCENT,
            TARGET_RETURN_PERSCENT_ST_DEV, TARGET_RETURN_PERSCENT_GAMES_NUMBER,
            TARGET_HIT);

    SlotMachine machine = configuration.generateSlotMashine(MIN_SYMBOLS_PER_CYLINDER, PRECIOUS_ST_DEV, MAX_ITERATIONS_NUMBER);

    bool repeate = true;
    char c = 0;
    while (repeate) {
        std::cout << "\n1 - Play\n" << "2 - Payback test\n" << "3 - Payback st.deviation test\n" << "0 - Exit\n";
        std::cin.clear();
        std::cin >> c;
        std::cout << "\n";

        switch (c) {
            case '1':
                machine.playAndPrint();
                break;
            case '2':
                runPaybackTest(machine);
                break;
            case '3':
                runPaybackStDevTest(machine);
                break;
            case '0':
                repeate = false;
        }
    };
}

void runPaybackTest(SlotMachine &machine) {
    int games = 300000;

    std::string input;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter number of games (default 300000): ";
    std::getline(std::cin, input);
    if (input.length() > 0) {
        std::stringstream(input) >> games;
    }

    std::cout << "Games number for test: " << games << std::endl;

    double payBack;
    int hits;
    runTest(machine, games, payBack, hits);

    std::cout << "payback percent: " << payBack / games / BET << '\n';
    std::cout << "hits : " << (double) hits / games << '\n';
}

void runPaybackStDevTest(SlotMachine &machine) {
    int games = 300000;
    int sets = 20;

    std::string input;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter number of games (default 300000): ";
    std::getline(std::cin, input);
    if (input.length() > 0) {
        std::stringstream(input) >> games;
    }

    std::cout << "Games number: " << games  << std::endl;
    std::cout << "Sets number: " << sets  << std::endl;

    double payBack;
    int hits;
    
    std::vector<double> paybackPercentVector(sets);
    double  paybackPercentSum = 0;
    for (int i = 0; i < sets; i++) {
        runTest(machine, games, payBack, hits);
        double paybackPercent = payBack / games / BET;
        paybackPercentSum += paybackPercent;
        paybackPercentVector[i] = paybackPercent;
        std::cout << (i + 1) << " " << paybackPercent << '\n';
    }
    
    double paybackPercentMean = paybackPercentSum / sets;
    double paybackPercentStDevSum = 0;
    for (double paybackPercent : paybackPercentVector) {
        paybackPercentStDevSum += pow(paybackPercentMean - paybackPercent, 2);
    }

    std::cout << "payback percent st. deviation: " << sqrt(paybackPercentStDevSum / sets) << '\n';
}

void runTest(SlotMachine &machine, int games, double &payBack, int &hits) {
    payBack = 0;
    hits = 0;

    for (int i = 0; i < games; i++) {
        double gain = machine.play();
        payBack += gain;
        hits += (gain > 0);
    }
}

void runTest2(SlotMachine &machine, int games) {
    int tries = 100;
    std::vector<double> pbPsVector;
    double pbPsMean = 0;
    for (int i = 0; i < tries; i++) {
        double gain = 0;
        for (int j = 0; j < games; j++) {
            gain += machine.play();
        }
        double pbPs = gain / games / BET;
        pbPsMean += pbPs;
        pbPsVector.push_back(pbPs);
    }
    pbPsMean /= tries;
}

/////////

void development() {
    LineSet lineSet(CYLINDERS_NUMBER, WINDOW_SIZE, sizeof (ENCODED_LINES) / sizeof (int), &ENCODED_LINES[0]);
    ScoresTable scoreTable(CYLINDERS_NUMBER, SCORES[0]);
    Configuration configuration(scoreTable, lineSet, BET,
            TARGET_RETURN_PERSCENT,
            TARGET_RETURN_PERSCENT_ST_DEV, TARGET_RETURN_PERSCENT_GAMES_NUMBER,
            TARGET_HIT);

    SlotMachine machine = configuration.generateSlotMashine(MIN_SYMBOLS_PER_CYLINDER, PRECIOUS_ST_DEV, MAX_ITERATIONS_NUMBER);


}

