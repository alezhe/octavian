#ifndef SCORESTABLE_H
#define SCORESTABLE_H

#include <mem.h>

#include "data.h"

class ScoresTable {
public:
    ScoresTable(unsigned int cylindersNumber, const double * scores);
    virtual ~ScoresTable();
private:
    const unsigned int cylindersNumber_;
    double (* const scores_)[SYMBOLS_NUMBER];
public:
    unsigned int getCylindersNumber() const;
    double getScore(Symbol symbol, int matchsNumber) const;
};

#endif /* SCORESTABLE_H */

