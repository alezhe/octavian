#include "Mashine.h"

// <editor-fold defaultstate="collapsed" desc="CONSTRUCTION">

SlotMachine::SlotMachine(const ScoresTable &scoresTable, LineSet &lineSet, std::vector<Cylinder> cylinders) :
scoresTable_(scoresTable), lineSet_(lineSet), cylinders_(cylinders),
cylindersNumber_(scoresTable.getCylindersNumber()), windowSize_(lineSet.getWindowSize()) {
    //TODO check weather ScoreTable and LineSet number of cylinders are equal
    SlotMachine::generator.seed(time(0));
}

SlotMachine::~SlotMachine() {
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="GETTERS">
int SlotMachine::getCylindersNumber() {
    return cylindersNumber_;
}
// </editor-fold>

double SlotMachine::play() {
    Symbol* combination = generateCombination();
    double gain = defineGain(combination);
    delete[] combination;
    return gain;
}

void SlotMachine::playAndPrint() {
    Symbol* combination = generateCombination();
    printCombination(combination);
    delete[] combination;
}

////////////////////

Symbol* SlotMachine::generateCombination() {
    Symbol *combination = new Symbol[cylindersNumber_ * windowSize_];
    for (int i = 0; i < cylindersNumber_; i++) {
        cylinders_[i].getRandomCombination(generator, windowSize_, combination + windowSize_ * i);
    }
    return combination;
}

double SlotMachine::defineGain(Symbol* combination) {
    double gain = 0;
    for (int s = 0; s < SYMBOLS_NUMBER; s++) {
        Symbol symbol = static_cast<Symbol>(s);
        int matchs = lineSet_.getMatches(combination, symbol);
        gain += scoresTable_.getScore(symbol, matchs);
    }
    return gain;
}

int SlotMachine::getMatches(Symbol* combination, Symbol target) {
    return lineSet_.getMatches(combination, target);
}


void SlotMachine::printCombination(Symbol* combination) {
    std::cout << "-------------\n";
    for (int s = 0; s < windowSize_; s++) {
        for (int c = 0; c < cylindersNumber_; c++) {
            std::cout << combination[c * windowSize_ + s] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "---" << defineGain(combination) << "----\n";
}

