#include "ScoresTable.h"

ScoresTable::ScoresTable(unsigned int cylindersNumber, const double * scores) : cylindersNumber_(cylindersNumber), scores_(new double[cylindersNumber + 1][SYMBOLS_NUMBER]) {
    memcpy(scores_, scores, sizeof (double) * (cylindersNumber_ + 1) * SYMBOLS_NUMBER);
}

ScoresTable::~ScoresTable() {
    delete[] scores_;
}

unsigned int ScoresTable::getCylindersNumber() const {
    return cylindersNumber_;
}

double ScoresTable::getScore(Symbol symbol, int matchsNumber) const {
    return scores_[matchsNumber][symbol];
}