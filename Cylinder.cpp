#include "Cylinder.h"
#include "Mashine.h"

// <editor-fold defaultstate="collapsed" desc="CONSTRUCTION">
Cylinder::Cylinder(int (&distr)[SYMBOLS_NUMBER])
: size_(std::accumulate(distr, distr + SYMBOLS_NUMBER, 0)),
uid_(std::uniform_int_distribution<int>(1, std::accumulate(distr, distr + SYMBOLS_NUMBER, 0))) {
    symbols_ = new Symbol[size_];
    fillArray(distr);
    arangeArray();

//    printArray();
}

Cylinder::Cylinder(const Cylinder& orig) : size_(orig.size_), uid_(orig.uid_) {
    symbols_ = new Symbol[size_];
    std::copy(orig.symbols_, orig.symbols_ + size_, symbols_);
}

//Cylinder& Cylinder::operator = (const Cylinder& orig) {
//    size_ = orig.size_;
//    symbols_ = new Symbol[size_];
//    std::copy(orig.symbols_, orig.symbols_ + size_, symbols_);
//}

Cylinder::~Cylinder() {
    delete[] symbols_;
}

void Cylinder::fillArray(int (&distr)[SYMBOLS_NUMBER]) {
    std::fill(symbols_, symbols_ + size_, NO_SYMBOL);
    for (int i = 0; i < SYMBOLS_NUMBER; i++) {
        int position, counter = distr[i];
        while (counter > 0) {
            if (symbols_[position = rand() % size_] == NO_SYMBOL) {
                symbols_[position] = static_cast<Symbol> (i);
                --counter;
            }
        }
    }
}

void Cylinder::arangeArray() {
    bool onemore = false;
    for (int i = 0; i < size_; i++) {
        for (int j = 1; j < 3; j++) {
            int cmp = (i + j) % size_;
            if (symbols_[i] == symbols_[cmp]) {
                change(cmp);
                onemore = true;
            }
        }
        if (i == size_ - 1 && onemore) {
            onemore = false;
            i = -1;
        }
    }
}


void Cylinder::change(int i) {
    int j = i;
    int cmp1 = (i - 1 + size_) % size_;
    int cmp2 = (i - 2 + size_) % size_;
    do {
        j = (j + 1) % size_;
    } while (symbols_[j] == symbols_[cmp1] || symbols_[j] == symbols_[cmp2]);
    Symbol tmp = symbols_[i];
    symbols_[i] = symbols_[j];
    symbols_[j] = tmp;
}
// </editor-fold>

void Cylinder::printArray() {
    for (int i = 0; i < size_; i++) {
        std::cout << symbols_[i] << "-";
    }
    std::cout << "\n[";
    for (int i = 0; i < SYMBOLS_NUMBER; i++) {
        int count = 0;
        for (int j = 0; j < size_; j++) {
            count += symbols_[j] == i;
        }
        std::cout << count << ",";
    }

    std::cout << "]\n";
}

void Cylinder::getRandomCombination(std::mt19937 &generator, int n, Symbol * ar) {
    int pos = uid_(generator);
    for (int i = 0; i < n; i++) {
        ar[i] = symbols_[(pos + i) % size_];
    }

}

std::ostream& operator<<(std::ostream& os, const Cylinder& cylinder) {
    os << "[";
    for (int i = 0; i < cylinder.size_; i++) {
        os << cylinder.symbols_[i] << ',';
    }
    os << "]\n";
}