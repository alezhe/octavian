#include "Line.h"

// <editor-fold defaultstate="collapsed" desc="Line:CONSTRUCTION">

LineSet::Line::Line(LineSet * const pLineSet, long long lineCode, int combinationCode) : pLineSet_(pLineSet), lineCode_(lineCode), combinationCode_(combinationCode) {
    lineLength_ = defineLineLength();
}

LineSet::Line::Line(LineSet * const pLineSet, long long lineCode) : Line(pLineSet, lineCode, pLineSet -> codeCounter) {
    (pLineSet -> codeCounter) <<= 1;
}

LineSet::Line::Line(const Line& orig) : Line(orig.pLineSet_, orig.lineCode_, orig.combinationCode_) {
}

LineSet::Line::~Line() {
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Line:SERVICES">

LineSet::Line* LineSet::Line::conjuctWithLine(Line *pLine) {
    return new Line(pLineSet_, lineCode_ & pLine -> lineCode_, combinationCode_ | pLine -> combinationCode_);
}

int LineSet::Line::defineLineLength() {
    int i = 0;
    for (; i < pLineSet_ -> cylindersNumber_; i++) {
        long long cylindrCode = lineCode_ >> (i * pLineSet_ -> windowSize_) & 7; //TODO make mask
        if (cylindrCode == 0) {
            break;
        }
    }
    return i;
}

int LineSet::Line::getPossibleLength() {
    return lineLength_;
}

int LineSet::Line::getCombinationCode() {
    return combinationCode_;
}

int LineSet::Line::getPositionInWindow(int c) {
    int window = lineCode_ >> (c * pLineSet_ -> windowSize_) & 7; //TODO make mask
    for (int i = 0; i < pLineSet_ -> windowSize_; i++) {
        if (window & 1 == 1) {
            return i;
        }
        window >>= 1;
    }
    return -1;
}
// </editor-fold>

std::ostream& operator<<(std::ostream& os, const LineSet::Line& line) {
    os << std::bitset<5> (line.combinationCode_) << " " << std::bitset<15> (line.lineCode_) << "\n";
}

////////////////////

// <editor-fold defaultstate="collapsed" desc="LineSet:CONSTRUCTION">

LineSet::LineSet(unsigned int cylindersNumber, unsigned int windowSize) : cylindersNumber_(cylindersNumber), windowSize_(windowSize) {
}

LineSet::LineSet(unsigned int cylindersNumber, unsigned int windowSize, unsigned int linesNumber, const int *codedLines) : LineSet(cylindersNumber, windowSize) {
    for (int i = 0; i < linesNumber; i++) {
        addLine(codedLines[i]);
    }
}

LineSet::~LineSet() {
    //TODO destructions of collections of lines
}

void LineSet::addLine(long long lineCode) {
    Line *pNewLine = new Line(this, lineCode);
    std::map<int, std::vector < Line*>> newLines;
    newLines[0].push_back(pNewLine);
    for (std::pair<int, std::vector < Line*>> pair : lines_) {
        for (Line *pLine : pair.second) {
            Line *pConjuctedLine = pNewLine -> conjuctWithLine(pLine);
            newLines[pair.first + 1].push_back(pConjuctedLine);
        }
    }
    for (std::pair<int, std::vector < Line*>> pair : newLines) {
        for (Line *pLine : pair.second) {
            lines_[pair.first].push_back(pLine);
        }
    }
}

void LineSet::printSet() {
    for (std::pair<int, std::vector < Line*>> pair : lines_) {
        std::cout << pair.first << "\n";
        for (Line *pLine : pair.second) {
            std::cout << *pLine;
        }
        std::cout << "\n";
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="GETTERS">

int LineSet::getWindowSize() {
    return windowSize_;
}
// </editor-fold>

bool LineSet::isAppropriateSymbolDistribution(int (*distr)[SYMBOLS_NUMBER]) {
    for (int c = 0; c < cylindersNumber_; c++) {
        int symbolsNumberInCylinder = std::accumulate(distr[c], distr[c] + SYMBOLS_NUMBER, 0);
        for (int s = 0; s < SYMBOLS_NUMBER; s++) {
            if ((distr[c][s] < 1) || symbolsNumberInCylinder / distr[c][s] < windowSize_) {
                return false;
            }
        }
    }
    return true;
}

double (*LineSet::calculateProbabilities(int (*distr)[SYMBOLS_NUMBER]))[SYMBOLS_NUMBER] {
    double (*prob)[SYMBOLS_NUMBER] = new double[cylindersNumber_ + 1][SYMBOLS_NUMBER];
    int * symbolsNumbers = countSymbolsInCylinders(distr);
    unsigned long long totalPermutationsNumber = countTotalPermutationsNumber(symbolsNumbers);

    for (int d = 0; d <= cylindersNumber_; ++d) {
        std::vector<LineSet::Line*> matchLines = defineLinesCombinations(d);
        for (int s = 0; s < SYMBOLS_NUMBER; ++s) {
            unsigned long long permNmb = countPermutationsNumber(distr, symbolsNumbers, static_cast<Symbol> (s), d, matchLines);
            prob[d][s] = (double) permNmb / totalPermutationsNumber;
        }
    }

    delete[] symbolsNumbers;
    return prob;
}

// <editor-fold defaultstate="collapsed" desc="LineSet:PROBABILITY CALCULATION">

std::vector<LineSet::Line*> LineSet::defineLinesCombinations(int matchs) {
    int selectedCode = 0;
    std::vector<Line*> chosen;

    std::map<int, std::vector < Line*>>::reverse_iterator it = lines_.rbegin();
    for (; it != lines_.rend(); ++it) {
        for (Line *pLine : it -> second) {
            if (pLine -> getPossibleLength() >= matchs) {
                int linesCombinationCode = pLine -> getCombinationCode();
                if ((selectedCode & linesCombinationCode) == 0) {
                    chosen.push_back(pLine);
                    selectedCode |= linesCombinationCode;
                    if (selectedCode == 31) { //TODO make mask
                        return chosen;
                    }
                }
            }
        }
    }

    return chosen;
}

unsigned long long LineSet::countPermutationsNumber(int (*distr)[SYMBOLS_NUMBER], int * symbolsNumbers, Symbol symbol, int matchs, std::vector<LineSet::Line*> matchLines) {
    unsigned long long totalPermutationsNumber = 0;
    for (Line *pLine : matchLines) {
        unsigned long long permutationsNumber = 1;
        int c = 0;
        for (; c < matchs; ++c) {
            permutationsNumber *= distr[c][symbol];
        }
        for (; c < cylindersNumber_; ++c) {
            if (c == matchs) {
                permutationsNumber *= symbolsNumbers[c] - distr[c][symbol] * defineForbidenSymbolsNumber(pLine, matchs);
            } else {
                permutationsNumber *= symbolsNumbers[c];
            }
        }

        totalPermutationsNumber += permutationsNumber;
    }
    return totalPermutationsNumber;
}

int * LineSet::countSymbolsInCylinders(int (*distr)[SYMBOLS_NUMBER]) {
    int * symbolsCounters = new int[cylindersNumber_];
    for (int c = 0; c < cylindersNumber_; ++c) {
        symbolsCounters[c] = std::accumulate(distr[c], distr[c] + SYMBOLS_NUMBER, 0);
    }
    return symbolsCounters;
}

unsigned long long LineSet::countTotalPermutationsNumber(int* symbolsNumbers) {
    unsigned long long n = 1;
    for (int* p = symbolsNumbers; p < symbolsNumbers + cylindersNumber_; ++p) {
        n *= (*p);
    }
    return n;
}

int LineSet::defineForbidenSymbolsNumber(Line *pLine, int matchs) { //TODO improve
    int number = 0;
    int lineNumber = pLine -> getCombinationCode();
    while (lineNumber > 0) {
        if ((lineNumber & 1) == 1)
            number++;
        lineNumber >>= 1;
    }
    return number > windowSize_ ? windowSize_ : number;
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="GAME">

int LineSet::getMatches(Symbol* combination, Symbol target) {
    int maxCount = 0;
    for (Line *pLine : lines_[0]) {
        int counter = 0;
        for (int c = 0; c < cylindersNumber_; c++) {
            int position = pLine -> getPositionInWindow(c);
            if (combination[c * windowSize_ + position] == target) {
                ++counter;
            } else {
                break;
            }
        }
        maxCount = (counter > maxCount) ? counter : maxCount;
    }
    return maxCount;
}
// </editor-fold>
