#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <math.h>
#include <numeric>
#include <cstring>
#include <vector>

#include "data.h"
#include "Mashine.h"
#include "ScoresTable.h"
#include "Line.h"

class Configuration {
public:
    Configuration(ScoresTable &scoresTable, LineSet &lineSet);
    Configuration(ScoresTable &scoresTable, LineSet &lineSet, double bet, double paybackPercentExp, double paybackPercentStandDev, int gamesNumber, double hitExp);
    virtual ~Configuration();
private:
    
    const ScoresTable &scoresTable_;
    LineSet &lineSet_;
    const int cylindersNumber_;
    
    double bet_ = 5;
    double paybackPercentExpectation_ = 0.97;
    double paybackPercentStandDeviation_ = 0.007;
    int paybackPercentGamesNumber_ = 300000;
    double hitExpectation_ = 0.2;
    double correction = 1.0;
    
    double targetPaybackExpect_;
    double targetPaybackDisp_;
    double targetHitExp_;
public: //SETTERS
    void setTargetBet(double bet);
    void setTargetPaybackPercentExpectation(double expectation);
    void setTargetPaybackPersentStandDev(double standDev, int gamesNumber);
    void setTargetHitExp(double expectation);
private:
    void setTargetVariablesValues();
public: //GENERATORS
    SlotMachine generateSlotMashine(int minSymbolsPerCylinder, double precision, int tries);
private: //GENERATORS (!)must be privet
    int (*generateSymbolsDistributions(int minSymbolsPerCylinder, double precision, int tries))[SYMBOLS_NUMBER];
    bool findOptimisatedDistribution(int (*distr)[SYMBOLS_NUMBER], int (*optDistr)[SYMBOLS_NUMBER], double &minAapproach);
    int (*generateDistributionsApproximation(int symbolsPerCylinder))[SYMBOLS_NUMBER];
private: //TARGET VALUES CALCULATIONS
    void calculateTargetVariables(int (*distr)[SYMBOLS_NUMBER], double &gainExp, double &gainDisp, double &hitExp);
    void calculateExpectations(double (*probabilities)[SYMBOLS_NUMBER], double(*scoreToRandomVariableFunc)(double), double &expectation);
    double estimateApproachToTargetValues(const double &gainExp, const double &gainDisp, const double &hitExp);
private: //OTHER
    void printDistr(int (*distr)[SYMBOLS_NUMBER]);
    void printConfiguration();
    void printMachineParameters(int minSymbolsPerCylinder, double precision, int tries);
    void printCalculatedParameters(int (*distr)[SYMBOLS_NUMBER]);
};

#endif /* CONFIGURATION_H */

