#ifndef CYLINDER_H
#define CYLINDER_H

#include <iostream>
#include <ctime>
#include <random>
#include <stdlib.h>

#include "data.h"

class Cylinder {
private:
    const int size_;
    std::uniform_int_distribution<int> uid_;
    Symbol * symbols_;
public:
    Cylinder(int (&distr)[SYMBOLS_NUMBER]);
    Cylinder(const Cylinder& orig);
//    Cylinder& operator = (const Cylinder& orig);
    virtual ~Cylinder();
private:
    void fillArray(int (&distr)[SYMBOLS_NUMBER]);
    void arangeArray();
    void change(int i);
    void printArray();
public:
    void getRandomCombination(std::mt19937 &generator, int n, Symbol * ar);
    
    friend std::ostream& operator<<(std::ostream& os, const Cylinder& cylinder);
};

#endif /* CYLINDER_H */

